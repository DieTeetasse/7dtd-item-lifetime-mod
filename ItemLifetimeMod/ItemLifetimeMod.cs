﻿/*
Item Lifetime Mod
by Die Teetasse
XML file handling code by dmustanger
*/

using System;
using System.IO;
using System.Xml;
using UnityEngine;

namespace ItemLifetimeMod
{
    public class ItemLifetime : IModApi
    {
        // current item lifetime in seconds
        private static int lifetimeValue = 5;

        // config file
        private static string configFileName = "itemlifetimemod.xml";
        private static string configFile = string.Format("{0}/{1}", GamePrefs.GetString(EnumGamePrefs.SaveGameFolder), configFileName);
        private static FileSystemWatcher configFileWatcher = new FileSystemWatcher(GamePrefs.GetString(EnumGamePrefs.SaveGameFolder), configFileName);

        // sets a new lifetime value and saves it
        public static void setLiftimeValue(int value)
        {
            lifetimeValue = value;
            SaveXML();
        }

        // server dll hooked function 
        // change lifetime parameter before the execution of the original function
        public static bool GameManager_ItemDropServer(ref ItemStack _itemStack, ref Vector3 _dropPos, ref Vector3 _randomPosAdd, ref Vector3 _initialMotion, ref int _entityId, ref float _lifetime, ref bool _bDropPosIsRelativeToHead)
        {
            // when zero stop execution
            if (lifetimeValue == 0)
            {
                return false;
            }

            // when negative dont change the lifetime parameter
            if (lifetimeValue > -1)
            {
                _lifetime = lifetimeValue;
            }
            return true;
        }

        // mod api init mod
        public void InitMod()
        {
            ModEvents.GameStartDone.RegisterHandler(GameAwake);
            ModEvents.GameShutdown.RegisterHandler(GameShutdown);
        }

        // mod event game awake
        // load xml file at startup and start file watcher
        public void GameAwake()
        {
            // load xml
            LoadXML();

            // init config file watcher
            configFileWatcher.Changed += new FileSystemEventHandler(OnFileChanged);
            configFileWatcher.Created += new FileSystemEventHandler(OnFileChanged);
            configFileWatcher.Deleted += new FileSystemEventHandler(OnFileChanged);
            configFileWatcher.EnableRaisingEvents = true;
        }

        // mod event game shutdown
        // remove file watcher
        public void GameShutdown()
        {
            configFileWatcher.Dispose();
        }

        // filewatcher file change function
        // reload xml
        private static void OnFileChanged(object source, FileSystemEventArgs e)
        {
            LoadXML();
        }

        // load xml file or create one
        private static void LoadXML()
        {
            if (File.Exists(configFile))
            {
                // load xml
                try
                {
                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.Load(configFile);
                    XmlNode xmlNode = xmlDoc.DocumentElement.FirstChild;
                    if (xmlNode.Name == "config")
                    {
                        xmlNode = xmlNode.FirstChild;
                        if (xmlNode.Name == "lifetime")
                        {
                            XmlElement xmlElement = (XmlElement)xmlNode;
                            string value = xmlElement.GetAttribute("value");
                            int.TryParse(value, out lifetimeValue);
                            Log.Out(string.Format("[ItemLifetimeMod] Max item lifetime set to {0}.", lifetimeValue));
                        }
                    }

                }
                catch (Exception e)
                {
                    // just do nothing
                    Log.Error(string.Format("[ItemLifetimeMod] An error occured during the load of xml config file {0}. Error: {1}", configFile, e.Message));
                    return;
                }
            }
            else
            {
                // create xml
                SaveXML();
            }
        }

        // save xml file
        private static void SaveXML()
        {
            configFileWatcher.EnableRaisingEvents = false;
            using (StreamWriter sw = new StreamWriter(configFile))
            {
                sw.WriteLine("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
                sw.WriteLine("<itemlifetimemod>");
                sw.WriteLine("    <config>");
                sw.WriteLine(string.Format("        <lifetime value=\"{0}\" />", lifetimeValue));
                sw.WriteLine("    </config>");
                sw.WriteLine("</itemlifetimemod>");
                sw.Flush();
                sw.Close();
            }
            configFileWatcher.EnableRaisingEvents = true;
        }
    }
}
