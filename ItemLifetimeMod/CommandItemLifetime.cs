﻿/*
Item Lifetime Mod
by Die Teetasse
*/

using System.Collections.Generic;

namespace ItemLifetimeMod
{
    class CommandItemLifetime : ConsoleCmdAbstract
    {
        public override string GetDescription()
        {
            return "Set the lifetime of every dropped item (in seconds). Set to 0 to disable item spawning. Set to -1 to disable this feature.";
        }

        public override string GetHelp()
        {
            return "Set the lifetime of every dropped item (in seconds).\n" + 
                   "Usage:\n" +
                   "lft 5 (after 5 seconds the dropped item will be removed)\n" +
                   "lft 0 (no dropped item will be spawned)\n" +
                   "lft -1 (disable this feature)";
        }

        public override string[] GetCommands()
        {
            return new string[] { "itemlifetime", "lft" };
        }

        public override void Execute(List<string> _params, CommandSenderInfo _senderInfo)
        {
            // 1 parameter?
            if (_params.Count != 1)
            {
                SdtdConsole.Instance.Output(string.Format("Wrong number of arguments (expected: 1, found: {0}).", _params.Count));
                return;
            }
            // try to parse integer and set the value
            int value = 0;
            if (!int.TryParse(_params[0], out value))
            {
                SdtdConsole.Instance.Output("The argument is not an integer.");
                return;
            }
            ItemLifetime.setLiftimeValue(value);
            SdtdConsole.Instance.Output(string.Format("Max item lifetime set to {0}.", value));
        }
    }
}
