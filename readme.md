# 7dtd - Item Lifetime Mod #

Via a xml file or the command 'itemlifetime' / 'lft' the lifetime (in seconds) of dropped items can by dynamically adjusted.
Setting a negative value will deactivate the feature.
Setting the value to 0 will disable the spawning of dropped items entirely.

### Compiling ###
To compile the project you need these files in the 'dlls' folder:

* Assembly-CSharp.dll
* Assembly-CSharp-firstpass.dll
* LogLibrary.dll
* System.dll
* System.Xml.dll
* UnityEngine.dll
* UnityEngine.CoreModule.dll

The project will be compiled to the 'output' folder.

### Installation ###
* Stop the server
* Copy the folder 'ItemLifetimeMod' into '7 Days to Die Dedicated Server\Mods'
* Copy these files into the same folder as 'ItemLifetimeMod_Patcher.exe':
	* Assembly-CSharp.dll
	* UnityEngine.CoreModule.dll
	* UnityEngine.PhysicsModule.dll
* Execute 'ItemLifetimeMod_Patcher.exe'
* Copy the file 'Assembly-CSharp.patched.dll' into '7 Days to Die Dedicated Server\7DaysToDieServer_Data\Managed'
* Rename the file 'Assembly-CSharp.dll' in the same folder (so you can restore it if needed)
* Rename the file 'Assembly-CSharp.patched.dll' to 'Assembly-CSharp.dll'
* Start the server

### Releases ###
* v1.5: https://bitbucket.org/DieTeetasse/7dtd-item-lifetime-mod/downloads/ItemLifetimeMod%20v1.5.zip
* v1.4: https://bitbucket.org/DieTeetasse/7dtd-item-lifetime-mod/downloads/ItemLifetimeMod%20v1.4.zip
* v1.3: https://bitbucket.org/DieTeetasse/7dtd-item-lifetime-mod/downloads/ItemLifetimeMod%20v1.3.zip
* v1.2: https://bitbucket.org/DieTeetasse/7dtd-item-lifetime-mod/downloads/ItemLifetimeMod%20v1.2.zip

### Changelog ###
* v1.5
	* Updated to a17 (b240)
* v1.4
	* Updated to a17 (b199)
* v1.3
	* Updated to a16