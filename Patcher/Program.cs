﻿/*
Item Lifetime Mod Patcher
by Die Teetasse
Original patching concept and code by Alloc
*/
using System;
using System.Reflection;
using Mono.Cecil;
using Mono.Cecil.Cil;
using ItemLifetimeMod;

namespace Patcher
{
    class ItemLifetimeModPatcher
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ItemLifetimeMod Patcher by Die Teetasse (Concept by Alloc)");

            ModuleDefinition module = ModuleDefinition.ReadModule("Assembly-CSharp.dll");
            TypeDefinition type = module.GetType("GameManager");
            addControlledHook(type, "ItemDropServer", 7, false, true, true, typeof(ItemLifetime).GetMethod("GameManager_ItemDropServer"));
            module.Write("Assembly-CSharp.patched.dll");

            Console.WriteLine("Done! Next steps:");
            Console.WriteLine("1. Stop the server");
            Console.WriteLine("2. Copy the folder 'ItemLifetimeMod' into '7 Days to Die Dedicated Server\\Mods'");
            Console.WriteLine("3. Copy the file 'Assembly-CSharp.patched.dll' into '7 Days to Die Dedicated Server\\7DaysToDieServer_Data\\Managed'");
            Console.WriteLine("4. Rename the file 'Assembly-CSharp.dll' in the same folder (so you can restore it if needed)");
            Console.WriteLine("5. Rename the file 'Assembly-CSharp.patched.dll' to 'Assembly-CSharp.dll'");
            Console.WriteLine("6. Start the server");
            Console.WriteLine("");
            Console.WriteLine("Press Enter to exit.");
            Console.ReadLine();
        }

        // finds a method
        private static MethodDefinition findMethod(TypeDefinition type, string methodName, int parameterCount = -1)
        {
            foreach (MethodDefinition method in type.Methods)
            {
                if (method.Name.Equals(methodName) && (parameterCount == -1 || method.Parameters.Count == parameterCount))
                {
                    return method;
                }
            }

            Console.WriteLine("Could not find method " + type.FullName + "->" + methodName + " !");
            return null;
        }

        // replaces a method completly
        private static void replaceMethod(TypeDefinition type, string methodName, bool addThis, bool addParameter, MethodBase targetMethod)
        {
            replaceMethod(type, methodName, -1, addThis, addParameter, targetMethod);
        }

        private static void replaceMethod(TypeDefinition type, string methodName, int parameterCount, bool addThis, bool addParameter, MethodBase targetMethod)
        {
            MethodDefinition method = findMethod(type, methodName, parameterCount);
            if (method != null)
            {
                Console.WriteLine("Replace " + type.FullName + "->" + method.Name);
                var il = method.Body.GetILProcessor();
                // remove original code
                il.Body.Instructions.Clear();
                il.Body.ExceptionHandlers.Clear();
                // add this and parameters
                if (addThis)
                {
                    il.Emit(OpCodes.Ldarg, 0);
                }
                if (addParameter)
                {
                    for (int op = 0; op < method.Parameters.Count; op++)
                        il.Emit(OpCodes.Ldarg, op + 1);
                }
                // create new method call and return
                il.Emit(OpCodes.Call, method.Module.Import(targetMethod));
                il.Emit(OpCodes.Ret);
            }
        }

        // removes the code of an method
        private static void removeMethod(TypeDefinition type, string methodName, int parameterCount = -1)
        {
            MethodDefinition method = findMethod(type, methodName, parameterCount);
            if (method != null)
            {
                Console.WriteLine("Remove " + type.FullName + "->" + method.Name);
                var il = method.Body.GetILProcessor();
                // remove all code and add a return
                il.Body.Instructions.Clear();
                il.Body.ExceptionHandlers.Clear();
                il.Emit(OpCodes.Ret);
            }
        }

        // adds an hook to the method which can prevent the execution of the original code
        // the target method needs to return a boolean (true = execute original code / false = prevent execution)
        private static void addControlledHook(TypeDefinition type, string methodName, bool addThis, bool addParameter, bool parameterAsRef, MethodBase targetMethod)
        {
            addControlledHook(type, methodName, -1, addThis, addParameter, parameterAsRef, targetMethod);
        }

        private static void addControlledHook(TypeDefinition type, string methodName, int parameterCount, bool addThis, bool addParameter, bool parameterAsRef, MethodBase targetMethod)
        {
            MethodDefinition method = findMethod(type, methodName, parameterCount);
            if (method != null)
            {
                Console.WriteLine("AddControlledHook " + type.FullName + "->" + method.Name);
                var il = method.Body.GetILProcessor();
                var call = il.Create(OpCodes.Call, method.Module.Import(targetMethod));
                // add this and parameter
                var i = 0;
                if (addThis)
                {
                    il.InsertBefore(method.Body.Instructions[i++], il.Create(OpCodes.Ldarg, 0));
                }
                if (addParameter)
                {
                    for (int op = 0; op < method.Parameters.Count; op++)
                    {
                        if (parameterAsRef)
                        {
                            il.InsertBefore(method.Body.Instructions[i++], il.Create(OpCodes.Ldarga_S, method.Parameters[op]));
                        }
                        else
                        {
                            il.InsertBefore(method.Body.Instructions[i++], il.Create(OpCodes.Ldarg, op + 1));
                        }
                    }
                }
                // create "if(!call) return;"
                il.InsertBefore(method.Body.Instructions[i++], call);
                il.InsertBefore(method.Body.Instructions[i], il.Create(OpCodes.Brtrue_S, method.Body.Instructions[i++])); // jump to the code at this spot (= 1st instruction of the original code)
                il.InsertBefore(method.Body.Instructions[i++], il.Create(OpCodes.Ret));
            }
        }

        // adds an hook to the method
        private static void addHook(TypeDefinition type, string methodName, bool addThis, bool addParameter, bool parameterAsRef, bool atEnd, MethodBase targetMethod)
        {
            addHook(type, methodName, -1, addThis, addParameter, parameterAsRef, atEnd, targetMethod);
        }

        private static void addHook(TypeDefinition type, string methodName, int parameterCount, bool addThis, bool addParameter, bool parameterAsRef, bool atEnd, MethodBase targetMethod)
        {
            MethodDefinition method = findMethod(type, methodName, parameterCount);
            if (method != null)
            {
                Console.WriteLine("AddHook " + type.FullName + "->" + method.Name);
                var il = method.Body.GetILProcessor();
                var call = il.Create(OpCodes.Call, method.Module.Import(targetMethod));
                // insert at the end
                if (atEnd)
                {
                    // remove return
                    il.Remove(method.Body.Instructions[method.Body.Instructions.Count - 1]);
                    // add this and parameter
                    if (addThis)
                    {
                        il.Emit(OpCodes.Ldarg, 0);
                    }
                    if (addParameter)
                    {
                        for (int op = 0; op < method.Parameters.Count; op++)
                        {
                            if (parameterAsRef)
                            {
                                il.Emit(OpCodes.Ldarga_S, method.Parameters[op]);
                            }
                            else
                            {
                                il.Emit(OpCodes.Ldarg, op + 1);
                            }
                        }
                    }
                    // create call and return value
                    il.Append(call);
                    il.Emit(OpCodes.Ret);
                }
                else {
                    var i = 0;
                    // add this and parameters
                    if (addThis)
                    {
                        il.InsertBefore(method.Body.Instructions[i++], il.Create(OpCodes.Ldarg, 0));
                    }
                    if (addParameter)
                    {
                        for (int op = 0; op < method.Parameters.Count; op++)
                        {
                            if (parameterAsRef)
                            {
                                il.InsertBefore(method.Body.Instructions[i++], il.Create(OpCodes.Ldarga_S, method.Parameters[op]));
                            }
                            else
                            {
                                il.InsertBefore(method.Body.Instructions[i++], il.Create(OpCodes.Ldarg, op + 1));
                            }
                        }
                    }
                    // create call
                    il.InsertBefore(method.Body.Instructions[i++], call);
                }
            }
        }

        // prints out the code of a method
        private static void printBody(TypeDefinition type, string methodName, int parameterCount = -1)
        {
            MethodDefinition method = findMethod(type, methodName, parameterCount);
            if (method != null)
            {
                var il = method.Body.GetILProcessor();
                for (int ii = 0; ii < il.Body.Instructions.Count; ii++)
                {
                    Console.WriteLine(ii + " " + il.Body.Instructions[ii].ToString());
                }
            }
        }
    }
}
